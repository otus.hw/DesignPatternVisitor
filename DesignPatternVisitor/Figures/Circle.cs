﻿using DesignPatternVisitor.Export;
using System;
using System.Collections.Generic;
using System.Text;

namespace DesignPatternVisitor.Figures
{
    public class Circle : Shape, IElement
    {
        public readonly int radius;
        public Circle(int x, int y, int radius) : base(x, y)
        {
            this.radius = radius;
        }

        public string Export(IExport export)
        {
            return export.Export(this);
        }

    }
}
