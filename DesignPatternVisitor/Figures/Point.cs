﻿using DesignPatternVisitor.Export;
using System;
using System.Collections.Generic;
using System.Text;

namespace DesignPatternVisitor.Figures
{
    public class Point : Shape, IElement
    {
        public Point(int x, int y) : base (x,y)
        {

        }

        public string Export(IExport export)
        {
            return export.Export(this);
        }

    }
}
