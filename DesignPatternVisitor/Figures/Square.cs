﻿using DesignPatternVisitor.Export;
using System;
using System.Collections.Generic;
using System.Text;

namespace DesignPatternVisitor.Figures
{
    public class Square : Shape, IElement
    {
        public readonly int side;
        public Square(int x, int y, int side) : base(x, y)
        {
            this.side = side;
        }

        public string Export(IExport export)
        {
            return export.Export(this);
        }

    }
}
