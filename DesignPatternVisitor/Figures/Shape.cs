﻿using DesignPatternVisitor.Export;
using System;
using System.Collections.Generic;
using System.Text;

namespace DesignPatternVisitor.Figures
{
    public abstract class Shape 
    {
        public readonly int x, y;

        public Shape(int x, int y)
        {
            this.x = x;
            this.y = y;
        }

    }
}
