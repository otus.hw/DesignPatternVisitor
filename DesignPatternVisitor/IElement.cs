﻿using DesignPatternVisitor.Export;
using System;
using System.Collections.Generic;
using System.Text;

namespace DesignPatternVisitor
{
    public interface IElement
    {
        string Export(IExport export);
    }
}
