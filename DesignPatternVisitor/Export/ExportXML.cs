﻿using DesignPatternVisitor.Figures;
using System;
using System.Collections.Generic;
using System.Text;

namespace DesignPatternVisitor.Export
{
    class ExportXML : IExport
    {
        public string Export(Point shape)
        {
            return
$@"<POINT>
 <X>{shape.x}</X>
 <Y>{shape.y}</Y>
</POINT>";
        }

        public string Export(Circle shape)
        {
            return
$@"<CIRCLE>
 <X>{shape.x}</X>
 <Y>{shape.y}</Y>
 <RADIUS>{shape.radius}</RADIUS>
</CIRCLE>";
        }

        public string Export(Square shape)
        {
            return
$@"<SQUARE>
 <X>{shape.x}</X>
 <Y>{shape.y}</Y>
 <SIDE>{shape.side}</SIDE>
</SQUARE>";
        }
    }
}
