﻿using DesignPatternVisitor.Figures;
using System;
using System.Collections.Generic;
using System.Text;

namespace DesignPatternVisitor.Export
{
    class ExportJSN : IExport
    {
        public string Export(Point shape)
        {
            return $@"{{""x"":{shape.x}, ""y"":{shape.y}}}";
        }

        public string Export(Circle shape)
        {
            return $@"{{""x"":{shape.x}, ""y"":{shape.y}, ""radius"":{shape.radius}}}";
        }

        public string Export(Square shape)
        {
            return $@"{{""x"":{shape.x}, ""y"":{shape.y}, ""side"":{shape.side}}}";
        }
    }
}
