﻿using DesignPatternVisitor.Figures;
using System;
using System.Collections.Generic;
using System.Text;

namespace DesignPatternVisitor.Export
{
    public interface IExport
    {
        string Export(Circle shape);
        string Export(Square shape);
        string Export(Point shape);

    }
}
