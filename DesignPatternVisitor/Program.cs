﻿using DesignPatternVisitor.Export;
using DesignPatternVisitor.Figures;
using System;
using System.Drawing;

namespace DesignPatternVisitor
{
    class Program
    {
        static void Main(string[] args)
        {
            Figures.Point shape1 = new Figures.Point(10, 20);
            Circle shape2 = new Circle(10,20,30);
            Square shape3 = new Square(10,20,40);

            IExport exportXML = new ExportXML();
            IExport exportJSN = new ExportJSN();

            Console.WriteLine(shape1.Export(exportJSN));
            Console.WriteLine(shape2.Export(exportJSN));
            Console.WriteLine(shape3.Export(exportJSN));

            Console.WriteLine();

            Console.WriteLine(shape1.Export(exportXML));
            Console.WriteLine(shape2.Export(exportXML));
            Console.WriteLine(shape3.Export(exportXML));
        }
    }
}
